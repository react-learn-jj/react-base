The following is the guide of how was set up this project.
```
https://jscomplete.com/learn/1rd-reactful
```

This is the course I'm following to accomplish this react Results.
```
https://www.pluralsight.com/courses/react-js-getting-started
```


There are 3 commands to execute the app.

1. `npm run dev-sever` This is for running a server is being built and executed from react.
2. `dev:bundler` Running React app using development environment and of course development dependencies.
3. `prod:bundler` Running React app in production.


Check your application in the 4242 port: 
```
running in  http://localhost:4242/
```